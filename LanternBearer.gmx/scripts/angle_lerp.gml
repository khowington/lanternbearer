///angle_lerp(start_angle, target_angle, lerp);
var ang1 = argument0;
var ang2 = argument1;
var anglerp = argument2;

return ang1 - angle_difference(ang1, ang2) * anglerp;


global.dead = true

with oPlayer {
instance_destroy()

    if !instance_exists(oLantern_dead) {
    instance_create(x,y,oLantern_dead)
    }

    if !instance_exists(oDeathEffect) {
        instance_create(x,y,oDeathEffect)
    }  

//    if !onGround {
        if !instance_exists(oPlayerDeadAir) {
        instance_create(x,y,oPlayerDeadAir);
        }
//    }
/*
    if onGround {
        if !instance_exists(oPlayerDeadPain) {
        instance_create(x,y-1,oPlayerDeadPain);
        }
    }*/
}

if (file_exists("LanternBearer.sav"))
{
    ini_open("LanternBearer.sav");    
//    var LoadedRoom = ini_read_real("Save","room",Level_One);  
//game data
global.intro = ini_read_real("Save","intro",0);
//level data  
global.cavedata = ini_read_real("Save","cave",0);
global.jungledata = ini_read_real("Save","jungle",0);
global.desertdata = ini_read_real("Save","desert",0);
global.icedata = ini_read_real("Save","ice",0);
global.heckdata = ini_read_real("Save","heck",0);
//star
global.star_top = ini_read_real("Save","star_top",0);
global.star_top_right = ini_read_real("Save","star_top_right",0);
global.star_top_left = ini_read_real("Save","star_top_left",0);
global.star_bot_right = ini_read_real("Save","star_bot_right",0);
global.star_bot_left = ini_read_real("Save","star_bot_left",0);
//darkness
global.darkness = ini_read_real("Save","darkness",0.75);

ini_close();
//    room_goto(LoadedRoom);
}
else
{
    //do nothing
}

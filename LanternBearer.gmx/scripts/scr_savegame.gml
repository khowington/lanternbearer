if (file_exists("LanternBearer.sav")) file_delete("LanternBearer.sav");

ini_open("LanternBearer.sav");
//var SavedRoom = room;
//game data
ini_write_real("Save","intro",global.intro);
//level data
ini_write_real("Save","cave",global.cavedata);
ini_write_real("Save","jungle",global.jungledata);
ini_write_real("Save","desert",global.desertdata);
ini_write_real("Save","ice",global.icedata);
ini_write_real("Save","heck",global.heckdata);
//door data
ini_write_real("Save","caveDoor",global.caveDoor);
ini_write_real("Save","jungleDoor",global.jungleDoor);
ini_write_real("Save","desertDoor",global.desertDoor);
ini_write_real("Save","iceDoor",global.iceDoor);
ini_write_real("Save","heckDoor",global.heckDoor);
//star
ini_write_real("Save","star_top",global.star_top);
ini_write_real("Save","star_top_right",global.star_top_right);
ini_write_real("Save","star_top_left",global.star_top_left);
ini_write_real("Save","star_bot_right",global.star_bot_right);
ini_write_real("Save","star_bot_left",global.star_bot_left);
//darkness
ini_write_real("Save","darkness",global.darkness);

ini_close();



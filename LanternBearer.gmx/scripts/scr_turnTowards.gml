///scr_turnTowards(x, y, minSpeed, maxSpeed, steps)

var turn;
turn = 0;

targetDir = point_direction(x,y,argument0,argument1);
if(image_angle > 360) {
    image_angle -= 360;
}
if(image_angle < 0) {
    image_angle += 360;
}

if(targetDir > image_angle) {

    if(targetDir > image_angle+180){ //1.
        turn = -1*(360-targetDir+image_angle);
    } else { //2.
        turn = (targetDir-image_angle);
    }
    
} else {
    
    if(image_angle > targetDir+180) { //3.
        turn = (360-image_angle+targetDir);
    } else { //4.
        turn = (targetDir-image_angle);
    }

}

if(abs(image_angle-targetDir) <= argument2 || abs(image_angle-targetDir) >= 360-argument2) {
    image_angle = targetDir;
} else {

//Clamp max speed
    if(turn > 0 && turn > argument3*argument4) {
        turn = argument3*argument4;
    }
    if(turn < 0 && turn < -(argument3*argument4)) {
        turn = -(argument3*argument4);
    }
//Clamp min speed
    if(turn > 0 && turn < argument2*argument4) {
        turn = argument2*argument4
    }
    if(turn < 0 && turn > -(argument2*argument4)) {
        turn = -(argument2*argument4)
    }
    image_angle += turn/argument4;
}

///scr_view_zoom(amount, view)
var amount = 1+argument[0]
var view = argument[1]

//zoom limit
if (view_hport[view] <= 64 && amount < 1) exit;
if (view_yport[view] <= 1280 && amount < 1) exit;

//get offset
var offx = abs(view_hport[view]*amount - view_hport[view])
var offy = abs(view_yport[view]*amount - view_yport[view])

//scale view
view_hport = view_hport[view] * amount;
view_yport = view_yport[view] * amount;

//adjust view pos
if (amount < 1) {
    view_hport[view] += offx/2;
    view_yport[view] += offy/2;   
    }else if (amount > 1) {
    view_hport[view] -= offx/2;
    view_yport[view] -= offy/2;        
} 

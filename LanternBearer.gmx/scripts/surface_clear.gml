///surface_clear(surface,color,alpha)
surface_set_target(argument[0]);
draw_clear_alpha(argument[1],argument[2]);
surface_reset_target();
